__author__ = 'cesar17'
#!/usr/bin/env python

import sys

current_countryCode = None
current_count = 0
countryCode = None

# input comes from STDIN
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()

    # parse the input we got from mapper.py
    countryCode, count, time_interval = line.split('\t', 2)

    # convert count (currently a string) to int
    try:
        count = int(count)
    except ValueError:
        # count was not a number, so silently
        # ignore/discard this line
        continue

    # this IF-switch only works because Hadoop sorts map output
    # by key (here: word) before it is passed to the reducer
    if current_countryCode == countryCode:
        current_count += count
    else:
        if current_countryCode:
            # write result to STDOUT
            print '%s\t%s\t%s' % (current_countryCode, current_count, time_interval)
        current_count = count
        current_countryCode = countryCode

# do not forget to output the last word if needed!
if current_countryCode == countryCode:
    print '%s\t%s' % (current_countryCode, current_count)
